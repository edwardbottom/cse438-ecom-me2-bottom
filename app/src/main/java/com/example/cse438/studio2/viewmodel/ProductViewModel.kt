package com.example.cse438.studio2.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import android.util.Log
import com.example.cse438.studio2.model.Product
import com.example.cse438.studio2.util.QueryUtils

class ProductViewModel(application: Application): AndroidViewModel(application) {
    private var _productsList: MutableLiveData<ArrayList<Product>> = MutableLiveData()

    fun getNewProducts(): MutableLiveData<ArrayList<Product>> {
        //TODO: Implement this method based on LoopBack API's documentation and the below function getProductsByQueryText
        loadProducts("?filter={\"where\":{\"is_new\":1}}")
        return _productsList
    }

    fun getProductsByQueryText(query: String): MutableLiveData<ArrayList<Product>> {
        loadProducts("?filter={\"where\":{\"name\":{\"like\":\".*$query.*\",\"options\":\"i\"}}}")
        return _productsList
    }

    private fun loadProducts(query: String) {
        //TODO: Implement this function
        ProductAsyncTask().execute(query)
    }

    //TODO: Implement the AsyncTask below
    @SuppressLint("StaticFieldLeak")
    inner class ProductAsyncTask: AsyncTask<String, Unit, ArrayList<Product>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Product>? {
            return QueryUtils.fetchProductData(params[0]!!)
        }

        override fun onPostExecute(result: ArrayList<Product>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            }
            else {
                Log.e("RESULTS", result.toString())
                _productsList.value = result
            }
        }
    }
}